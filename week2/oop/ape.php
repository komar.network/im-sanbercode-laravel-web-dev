<?php

require_once('animal.php');

class Ape extends Animal
{

  protected $legs = 2;

  function yell()
  {
    echo "Auooo <br>";
  }
}