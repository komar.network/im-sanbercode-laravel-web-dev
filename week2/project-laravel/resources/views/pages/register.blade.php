<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register</title>
    <!-- Styles -->
    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>
</head>

<body>
    <h1>Buat Account Baru!</h1>

    <h2>Sign Up Form</h2>
    <form action="{{ route('welcome') }}" method="post">
        @csrf
        <label for="firstName">First Name:</label> <br><br>
        <input type="text" name="firstname"> <br><br>
        <label for="lastName">Last Name:</label> <br><br>
        <input type="text" name="lastname"> <br><br>
        <label for="gender">Gender:</label> <br><br>
        <input type="radio" value="Male" name="gender">Male <br>
        <input type="radio" value="Female" name="gender">Female <br>
        <input type="radio" value="Other" name="gender">Other <br><br>
        <label for="region">Nationality:</label> <br><br>
        <select name="region">
            <option disabled value="">Pilih Satu</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapure">Singapure</option>
        </select> <br><br>
        <label for="language">Language Spoken:</label> <br><br>
        <label for="bhsindo">Bahasa Indonesia</label>
        <input type="checkbox" value="Bahasa Indonesia" name="language"> <br>
        <label for="bhsinggris">English</label>
        <input type="checkbox" value="English" name="language"> <br>
        <label for="other">Other</label>
        <input type="checkbox" value="Other" name="language"> <br><br>
        <label for="textarea">Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <button type="submit">Sign Up</button>
    </form>
</body>

</html>
