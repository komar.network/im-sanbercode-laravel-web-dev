<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register()
    {
        return view('pages.register');
    }

    public function welcome(Request $request)
    {
        $firstName = $request->input('firstname');
        $lastName = $request->input('lastname');
        $fullName = $firstName . " " . $lastName;
        return view('pages.welcome', ['fullName' => $fullName]);
    }
}
