<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('table');
});
Route::get('/table', function () {
    return view('pages.table');
})->name('table');
Route::get('/data-tables', function () {
    return view('pages.data-tables');
})->name('data-tables');

// Tugas 15 CRUD
Route::get('/cast', [CastController::class, 'index'])->name('cast');
Route::get('/cast/create', [CastController::class, 'create'])->name('cast_create');
Route::post('/cast', [CastController::class, 'store'])->name('cast_store');
Route::get('/cast/{cast_id}', [CastController::class, 'show'])->name('cast_show');
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'])->name('cast_edit');
Route::put('/cast/{cast_id}', [CastController::class, 'update'])->name('cast_update');
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy'])->name('cast_destroy');